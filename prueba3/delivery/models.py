from django.db import models
from datetime import date


class Despacho(models.Model):
    numero_despacho = models.IntegerField(default=0)
    nombre_cliente = models.CharField(max_length=300)
    direccion = models.CharField(max_length=300)
    telefono = models.CharField(max_length=300)
    productos = models.CharField(max_length=300)
    peso = models.IntegerField(default=0)
    profundidad = models.IntegerField(default=0)
    ancho = models.IntegerField(default=0)
    alto = models.IntegerField(default=0)
    fecha_ingreso = models.DateField(default=date.today)
    fecha_envio = models.DateField(default=date.today)
    estado = models.CharField(max_length=20)

    def __str__(self):
        return self.numero_despacho, self.nombre_cliente
    