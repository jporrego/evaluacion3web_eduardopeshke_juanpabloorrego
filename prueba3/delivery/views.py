from django.shortcuts import render
from .models import Despacho
from django.http import HttpResponse

def home(request):
    return render(request, 'base.html')

def ingresar_datos(request):
    return render(request, 'ingresar.html')

def eliminar_despacho(request):
    return render(request, 'eliminar.html')

def cambiar_estado(request):
    return render(request, 'cambiar.html')

def listar(request):
    return render(request, 'listar.html')


